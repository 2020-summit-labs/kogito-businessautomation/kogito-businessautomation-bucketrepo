FROM docker.io/maven:3 as builder
WORKDIR /tmp

# summit-2020-kogito-businessautomation
RUN git clone https://gitlab.com/2020-summit-labs/kogito-businessautomation/kogito-travelagency.git && \
    cd /tmp/kogito-travelagency && \
    mvn clean install -DskipTests -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn de.qaware.maven:go-offline-maven-plugin:resolve-dependencies -DdownloadSources -DdownloadJavadoc -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn dependency:go-offline -B -Dmaven.repo.local=/tmp/bucketrepo && \
    ## 01-kogito-travel-agency
    cd /tmp/kogito-travelagency/01-kogito-travel-agency && \
    mvn clean install -DskipTests -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn de.qaware.maven:go-offline-maven-plugin:resolve-dependencies -DdownloadSources -DdownloadJavadoc -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn de.qaware.maven:go-offline-maven-plugin:resolve-dependencies -Pnative -DdownloadSources -DdownloadJavadoc -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn dependency:go-offline -B -Dmaven.repo.local=/tmp/bucketrepo && \
    ## 02-kogito-travel-agency
    cd /tmp/kogito-travelagency/02-kogito-travel-agency && \
    mvn clean install -DskipTests -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn de.qaware.maven:go-offline-maven-plugin:resolve-dependencies -DdownloadSources -DdownloadJavadoc -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn dependency:go-offline -B -Dmaven.repo.local=/tmp/bucketrepo && \
    ## 03-kogito-travel-agency
    cd /tmp/kogito-travelagency/03-kogito-travel-agency && \
    mvn clean install -DskipTests -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn de.qaware.maven:go-offline-maven-plugin:resolve-dependencies -DdownloadSources -DdownloadJavadoc -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn dependency:go-offline -B -Dmaven.repo.local=/tmp/bucketrepo && \
    ## 03-kogito-visas
    cd /tmp/kogito-travelagency/03-kogito-visas && \
    mvn clean install -DskipTests -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn de.qaware.maven:go-offline-maven-plugin:resolve-dependencies -DdownloadSources -DdownloadJavadoc -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn dependency:go-offline -B -Dmaven.repo.local=/tmp/bucketrepo && \
    cd /tmp/bucketrepo && \
    find -iname "*.repositories" -exec rm -f {} \;

FROM gcr.io/jenkinsxio/bucketrepo:0.1.19
COPY --from=builder /tmp/bucketrepo /tmp/bucketrepo